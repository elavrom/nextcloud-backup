Lancer ./prepare-backup.sh pour préparer le système pour faire des backups.
Le script va créer un jeu de clés SSH sur la machine BACKUP, et installer
clé publique sur la machine NEXTCLOUD. Le script demandera également les 
informations nécessaires à une connexion automatique à MySQL afin de sauvegarder 
la base de données. Une fois ceci fait, le script installera des tâches CRON pour
lancer les backups hebdomadaires (complètes) et quotidiennes (incrémentielles).

* Attention, cette opération demande le mot de passe de l'utilisateur.
* Il faut également le mot de passe root de MySQL pour faire les backups.
* Ce repo git doit être sur la machine backup et utilisée avec `user`
