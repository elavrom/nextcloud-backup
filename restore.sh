#!/usr/bin/env bash

installDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$installDir/env"

# date
if [[ -z $1 ]]; then
    date=`ls -U $backupRootDir | sort -nr | head -1`
else
    date=$1
fi

dirToRestore="$backupRootDir$date"

echo "Chemin de la sauvegarde à restaurer : $dirToRestore"

echo "Restauration des fichiers dans $nextcloudDir ..."

ssh nextcloud "sudo -u www-data /usr/bin/php /var/www/html/nextcloud/occ maintenance\:mode --on"

# Restore files
sudo /usr/bin/rsync -e "ssh -i /home/user/.ssh/id_rsa" --rsync-path="sudo /usr/bin/rsync" -Aa "$dirToRestore/nextcloud/" $nextcloudDir

echo "Restauration de la base de données..."
# Restore DB
scp "$dirToRestore/mysql/nextcloud.bak" nextcloud:/tmp/sql.bak
ssh nextcloud "mysql -e \"DROP DATABASE nextcloud\""
ssh nextcloud "mysql -e \"CREATE DATABASE nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci\""
ssh nextcloud "mysql nextcloud < /tmp/sql.bak"

ssh nextcloud "sudo -u www-data /usr/bin/php /var/www/html/nextcloud/occ maintenance\:mode --off"

echo "Restauration finie"
