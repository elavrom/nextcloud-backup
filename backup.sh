#!/usr/bin/env bash

installDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$installDir/env"

dt=`date +%Y%m%d`
backupDir="$backupRootDir${dt}/nextcloud/"
dbBackupDir="$backupRootDir${dt}/mysql/"
lastBackupToKeep=`date -d"today - $retentionDays days" +%Y%m%d`
lastComplete=`date -d"last-$completeDay" +Y%m%d`

type=$1
if [ -z $1 ]; then
    type="daily"
elif [ $1 != "daily" ]; then
    type="weekly"
fi

if [ ! -d "${backupDir}" ]
then
	mkdir -p "${backupDir}"
else
	echo "ERROR: The backup directory ${backupDir} already exists!"
	exit 1
fi


# Complete backup
ssh nextcloud "sudo -u www-data /usr/bin/php /var/www/html/nextcloud/occ maintenance\:mode --on"
sudo /usr/bin/rsync -e "ssh -i /home/user/.ssh/id_rsa" -az --rsync-path="sudo /usr/bin/rsync" $nextcloudDir $backupDir
ssh nextcloud "mysqldump --single-transaction -h localhost nextcloud > /tmp/nextcloud.bak"
mkdir -p $dbBackupDir
scp nextcloud:/tmp/nextcloud.bak ${dbBackupDir}nextcloud.bak
ssh nextcloud "sudo -u www-data /usr/bin/php /var/www/html/nextcloud/occ maintenance\:mode --off"



# Retention
for i in $(ls -d ${backupRootDir}/* | sed 's/[^0-9]*//g'); do
    if [ $i -gt $lastBackupToKeep ]; then
        break
    fi

    rm -rf "$backupRootDir$i"
done
