#!/usr/bin/env bash

installDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
home=~

dt=`date '+%Y%m%d_%H%M%S'`
path_to_dot_ssh="${home}/.ssh/"
k_priv_name=id_rsa
k_pub_name="${k_priv_name}.pub"

create_ssh_keys () {
    echo "Un jeu de clés SSH va être créé pour vous. (Emplacement: ${home}/.ssh/)"

    # Archive old keys
    if [[ -f ~/.ssh/id_rsa ]]; then
        cp ~/.ssh/id_rsa "${home}/.ssh/${dt}${priv}"
    fi
    if [[ -f ~/.ssh/id_rsa.pub ]]; then
        cp ~/.ssh/id_rsa "${home}/.ssh/${dt}${pub}"
    fi

    # Generate new keys
    ssh-keygen -f ~/.ssh/id_rsa -N ""
}

put_key_in_machine() {
    if [[ -z $1 ]]; then
        $1 = ~/.ssh/id_rsa.pub
    fi
    # Put it in nextcloud machine --> This will ask for user's password on nextcloud
    ssh-copy-id $1 nextcloud
}

read -p "Avez-vous déjà un jeu de clés SSH ? [o/n]" -n 1 -r
echo
if [[ $REPLY =~ ^[Oo]$ ]]; then
    read -p "Avez-vous déjà déposé votre clé publique sur la machine à sauvegarder ? [o/n]" -n 1 -r
    echo
    if [[ $REPLY =~ ^[^Oo]$ ]]; then
         read -p "Souhaitez-vous utiliser cette clé existante ? (Si vous répondez non, un nouveau jeu de clés sera créé) [o/n]" -n 1 -r
        echo
        if [[ $REPLY =~ ^[Oo]$ ]]; then
            read -p "Où se situe votre clé publique ?" -r pub
            put_key_in_machine pub
            echo
        else
            create_ssh_keys
            put_key_in_machine pub
        fi
    fi
else
    create_ssh_keys
    put_key_in_machine pub
fi
echo "Étape suivante ..."

read -p "Faut-il autoriser la connexion sans mot de passe à MySQL sur la machine nextcloud ? Nécessaire pour automatiser. (Pas obligatoire si ce script a déjà été lancé) [o/n]" -n 1 -r
echo

if [[ $REPLY =~ ^[Oo]$ ]]; then
    stty -echo
    printf "MySQL root's password:"
    read MY_ROOT_PASSWORD
    echo -e "# Fichier généré le : $(date)\n[client]\nuser=root\npassword=${MY_ROOT_PASSWORD}" > .my.cnf
    unset MY_ROOT_PASSWORD
    stty echo
    printf "\n"

    chmod 600 .my.cnf
    rsync -A .my.cnf nextcloud:~/.my.cnf
    rm .my.cnf
else
    echo "Étape suivante..."
fi

read -p "Faut-il ajouter les commandes sudoer ? Nécessaire pour automatiser. (Pas obligatoire si ce script a déjà été lancé) [o/n]" -n 1 -r
if [[ $REPLY =~ ^[Oo]$ ]]; then
    echo "Ajout de la commande de maintenance..."
    stty -echo
    printf "Votre mot de passe:"
    read NXT_PASSWORD
    stty echo
    printf "\n"
    echo -e "Cmnd_Alias RSYNC = /usr/bin/rsync\nuser ALL = NOPASSWD: RSYNC" > /tmp/sudoer
    echo ${NXT_PASSWORD} | sudo -S cp /tmp/sudoer /etc/sudoers.d/user
    echo ${NXT_PASSWORD} | sudo -S chown root:root /etc/sudoers.d/user
    echo -e "Cmnd_Alias MAINTENANCE_NEXTCLOUD = /usr/bin/php /var/www/html/nextcloud/occ maintenance\:mode *\nuser ALL = (www-data) NOPASSWD: MAINTENANCE_NEXTCLOUD" >> /tmp/sudoer
    scp /tmp/sudoer nextcloud:/home/user/user.sudo
    rm /tmp/sudoer
    ssh nextcloud "echo ${NXT_PASSWORD} | sudo -S mv /home/user/user.sudo /etc/sudoers.d/user 2>/dev/null"
    ssh nextcloud "echo ${NXT_PASSWORD} | sudo -S chown root:root /etc/sudoers.d/user 2>/dev/null"
    unset NXT_PASSWORD
fi

printf "\n"

read -p "Faut-il installer les tâches CRON ? Nécessaire pour automatiser. (Pas obligatoire si ce script a déjà été lancé) [o/n]" -n 1 -r
echo

if [[ $REPLY =~ ^[Oo]$ ]]; then
    # Récupérer le contenu de crontab
    crontab -l > mycron
    # Ajout de la tâche daily à la fin du fichier
    echo "0 0 * * * bash $installDir/backup.sh daily" >> mycron
    # Debug :
    # echo "*/10 * * * * bash $installDir/backup.sh daily > /home/user/log-`date +%Y%m%d.log` 2>&1" > mycron
    # Installer le nouveau fichier crontab
    crontab mycron
    rm mycron
fi
